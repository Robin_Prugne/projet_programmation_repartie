
/**
 * Interface ServiceWordSearcher
 * @author prugne2u aubert117u
 */
public interface ServiceWordSearcher extends java.rmi.Remote{
	
	public boolean search(String phrase, String mot) throws java.rmi.RemoteException;
}
