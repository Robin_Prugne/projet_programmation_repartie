import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Classe Distributeur
 * @author prugne2u aubert117u
 */

public class Distributeur implements ServiceDistributeur{
	
	/**
	 * 4 attrbitus :
	 * 	listeNoeuds - correspond � la liste des noeuds de calculs
	 * 	client - correspond au client connect�
	 * 	text - correspond au texte � analyser
	 * 	mot	- correspond au mot recherch� dans le texte
	 */
	
	private ArrayList<ServiceWordSearcher> listeNoeuds;
	private ServiceClient client;
	private String text;
	private String mot;
	
	/**
	 * Constructeur simple
	 */
	
	public Distributeur() {
		this.client = null;
		this.listeNoeuds = new ArrayList<ServiceWordSearcher>();
	}
	
	/**
	 * Methode ajouterNoeud
	 * Permet d'ajouter un noeud � la liste de noeud
	 */

	@Override
	public void ajouterNoeud(ServiceWordSearcher sws) throws RemoteException {
		this.listeNoeuds.add(sws);
		System.out.println("Nombre de noeuds connect�s : " +listeNoeuds.size());
	}
	
	/**
	 * Methode setClient
	 * @param sc
	 * 				Le client � attribuer
	 * Permet d'attribuer le client
	 */
	
	public void setClient(ServiceClient sc) {
		this.client = sc;
	}
	
	/**
	 * Methode distribuerPhrase
	 * @param tab 
	 * 				Tableau de chaine de caracteres correspodant aux differentes phrases
	 * @return
	 * 				Une liste contenant les phrases dans lesquelles le mot est pr�sent
	 * Permet de distribuer les phrases entre les noeuds
	 */
	
	@Override
	public ArrayList<String> distribuerPhrase(String[] tab) throws RemoteException {
		ArrayList<String> res = new ArrayList<String>();
		if(listeNoeuds.size() >= tab.length) {
			for(int i=0; i < listeNoeuds.size(); i++) {
				if(listeNoeuds.get(i).search(tab[i], this.mot)) {
					res.add(tab[i]);
				}
			}
		}else {
			int index = 0;
			int indexNoeud = 0;
			while(index < tab.length) {
				if(indexNoeud < listeNoeuds.size()) {
					if(listeNoeuds.get(indexNoeud).search(tab[index], this.mot)) {
						res.add(tab[index]);
					}
					index++;
					indexNoeud++;
				}else {
					indexNoeud = 0;
				
				}
			}
		}
		return res;
	}
	
	/**
	 * Methode start
	 * Permet de "demarrer" le processus
	 * @return
	 * 			Une liste contenant les phrases ou le mot est pr�sent
	 */
	
	public ArrayList<String> start() throws RemoteException {
		String[] phrases = this.cut();
		return this.distribuerPhrase(phrases);
	}
	
	/**
	 * Methode pour d�couper le texte en phrase
	 * @return
	 * 			Un tableau de String correspondant aux phrases
	 */
	
	public String[] cut() {
		return this.text.split(Pattern.quote("."));
    } 
	
	/**
	 * Methode setText
	 * @param t
	 * 			Le texte a attribuer
	 */
	
	public void setText(String t) {
		this.text = t;
	}
	
	/**
	 * Methode setMot
	 * @param
	 * 			Le mot a attribuer
	 */
	
	public void setMot(String m) {
		this.mot = m;
	}
}
