import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Classe LancerClient
 * @author prugne2u aubert117u
 */

public class LancerClient {
	
	/**
	 * Methode main
	 * @param args
	 * 				argument 1 : IP
	 * 				argument 2 : Chemin du fichier texte
	 * 				argument 3 : Mot � rechercher dans le texte
	 * 				argument 4 : Port (OPTIONNEL)
	 * @throws NotBoundException
	 * @throws IOException
	 */
	
	public static void main (String[] args) {
		String ip = args[0];
		String mot = args[2];
		int port = 1099;
		if(args.length > 3) {
			port = Integer.valueOf(args[3]);
		}
		try {
			System.out.println("Client lanc� ! \n");
			
			//Converti le fichier en string
			InputStream is = new FileInputStream(args[1]); 
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();
			
			while(line != null){ 
				sb.append(line).append("\n");
				line = buf.readLine(); 
			} 
			
			String text = sb.toString();
			
			//On instancie l'objet wordSearcher et client
			WordSearcher ws = new WordSearcher();
			Client c = new Client();
			
			//On r�cup�re l'annuaire
			Registry reg = LocateRegistry.getRegistry(ip, port);
			//On export l'objet wordSearcher et client
			ServiceWordSearcher stb = (ServiceWordSearcher) UnicastRemoteObject.exportObject(ws,0);
			ServiceClient sc = (ServiceClient) UnicastRemoteObject.exportObject(c,0);
			//On r�cup�re le serveur
			ServiceDistributeur rd = (ServiceDistributeur) reg.lookup("searcher");
			
			//On ajoute le client
			rd.setClient(sc);
			
			//On attribue le texte et le mot au serveur
			rd.setText(text);
			rd.setMot(mot);
			
			//On lance le processus
			ArrayList<String> liste = rd.start();
			//On r�cup�re les resultats
			c.getResult(liste);
			
			System.exit(0);
		}catch(NotBoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
