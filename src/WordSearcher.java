import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Classe WordSearcher
 * @author prugne2u aubert117u
 */

public class WordSearcher implements ServiceWordSearcher, Serializable{
	
	private boolean present;
	
	/**
	 * Methode search
	 * Permet de chercher si un mot est pr�sent dans la phrase
	 * @param phrase
	 * 				La phrase
	 * @param mot
	 * 				Le mot
	 * @return 
	 * 				Un booleen, si il est pr�sent ou non
	 */
	public boolean search(String phrase, String mot) {
	
		this.present = false;
		//On split la phrase pour avoir un tableau de mots
		String[] textTab = phrase.split(Pattern.quote(" "));
		for(int i=0; i<textTab.length; i++) {
			if(textTab[i].toLowerCase().contains(mot.toLowerCase())) {
				this.present = true;
			}
		}
		
		return this.present;
	}
	
	/**
	 * Methode isPresent
	 * @return
	 * 			Le booleen present, pour savoir si le mot est present dans le texte
	 */
	
	public boolean isPresent() {
		return this.present;
	}
}