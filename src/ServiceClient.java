import java.util.ArrayList;

/**
 * Interface ServiceClient
 * @author prugne2u aubert117u
 */

public interface ServiceClient extends java.rmi.Remote{
	
	public void getResult(ArrayList<String> liste) throws java.rmi.RemoteException;
}
