import java.util.ArrayList;

/**
 * Interface ServiceDistributeur
 * @author prugne2u aubert117u
 */

public interface ServiceDistributeur extends java.rmi.Remote {
	
	public void ajouterNoeud(ServiceWordSearcher sws) throws java.rmi.RemoteException;
	public ArrayList<String> distribuerPhrase(String[] tab) throws java.rmi.RemoteException;
	public ArrayList<String> start() throws java.rmi.RemoteException;
	public void setText(String t) throws java.rmi.RemoteException;
	public void setMot(String m) throws java.rmi.RemoteException;
	public void setClient(ServiceClient sc) throws java.rmi.RemoteException;
}
