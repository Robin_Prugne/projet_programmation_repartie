import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Classe Client
 * @author prugne2u aubert117u
 */

public class Client implements ServiceClient, Serializable{
	
	/**
	 * Methode getResult
	 * Permet d'afficher les resultats
	 * @param liste
	 * 				La liste contenant les phrases o� le mot est compris
	 */
	
	@Override
	public void getResult(ArrayList<String> liste) throws RemoteException {
		if(liste.size() != 0) {
			System.out.println("Le mot est pr�sent " +(liste.size()) +" fois dans le texte");
			System.out.println("Voici les phrases qui contiennent le mot : ");
			for(int i=0; i<liste.size(); i++) {
				System.out.println("  - " +liste.get(i));
			}
		}else {
			System.out.println("Le mot n'est pas pr�sent dans le texte");
		}
	}	
}