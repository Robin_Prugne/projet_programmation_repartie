import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe LancerNoeud
 * @author prugne2u aubert117u
 */

public class LancerNoeud {
	
	/**
	 * Methode main
	 * @param args
	 * 				argument 1 : IP
	 * 				argument 2 : Port (OPTIONNEL)
	 * @throws NotBoundException
	 * @throws IOException
	 */
	
	public static void main (String[] args){
		String ip = args[0];
		int port = 1099;
		if(args.length > 2) {
			port = Integer.valueOf(args[1]);
		}
	
		try {
			//On reprend la reference distante du distributeur
			Registry reg = LocateRegistry.getRegistry(ip, port);
			ServiceDistributeur rd = (ServiceDistributeur) reg.lookup("searcher");
			
			//On instancie l'objet wordsearcher
			WordSearcher ws = new WordSearcher();
			//On export cet objet
			ServiceWordSearcher sws = (ServiceWordSearcher) UnicastRemoteObject.exportObject(ws,0);
			//On ajoute au serveur le noeud
			rd.ajouterNoeud(sws);
			
			System.out.println("Le noeud a �t� ajout�");
			
		}catch(RemoteException e) {
			e.printStackTrace();
		}catch(NotBoundException e) {
			e.printStackTrace();
		}
	}
}
