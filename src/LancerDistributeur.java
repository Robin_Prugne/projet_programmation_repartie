import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe LancerDistributeur
 * @author prugne2u aubert117u
 */

public class LancerDistributeur {
	
	/**
	 * Methode main
	 * Pas d'arguments
	 * @param args
	 * @throws RemoteException
	 */
	
	public static void main(String[] args) {
		
		try {
			System.out.println("Le serveur est pret !");
			
			//On instancie le distributeur
			Distributeur distributeur = new Distributeur();
			//On export le distributeur
			ServiceDistributeur rd = (ServiceDistributeur) UnicastRemoteObject.exportObject(distributeur,0);
			Registry reg = LocateRegistry.getRegistry();
			reg.rebind("searcher",rd);
			
		}catch(RemoteException e) {
			e.printStackTrace();
		}
	}
}
